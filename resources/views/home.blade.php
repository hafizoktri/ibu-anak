@extends('index')

@section('title')
    Ibu Bersalin
@endsection

@section('content')

    <div class="py-4">
        <div>
            <h1 class="text-center">Selamat Datang di Aplikasi Ibu dan Anak</h1>
        </div>

        <section>
            <div class="text-center py-2">
                <img class="mx-3" src="https://asianparent-assets-id.dexecure.net/wp-content/uploads/sites/24/2017/11/1461282_644056482299606_518118312_n.jpg" alt="ibu" height="200">

                <img class="mx-3" src="https://awsimages.detik.net.id/visual/2016/02/15/873df034-87f8-446f-8a07-d9dbdfd4aa6e_169.jpg?w=650" alt="bayi" height="200">
            </div>

            <div class="text-center py-2">
                <iframe width="427" height="240" src="https://www.youtube.com/embed/LGrpsZ7BsQA?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                <iframe width="427" height="240" src="https://www.youtube.com/embed/-Z4jx5VMw8M?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </section>
    </div>

@endsection