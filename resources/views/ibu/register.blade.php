@extends('index')

@section('title')
    Registrasi Ibu Baru
@endsection

@section('content')
    <section class="container py-5">

        <div class="d-flex justify-content-center">
            <div class="card w-50">
                <div class="card-body">

                    <h3>Registrasi Ibu Baru</h3>

                    <form action="{{route('signup')}}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="font-weight-bold" >Nama</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="username" class="font-weight-bold">Username Ibu</label>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="email" class="font-weight-bold">Email</label>
                            <input type="email" name="email" id="email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password" class="font-weight-bold">Password Ibu</label>
                            <input type="text" name="password" id="password" class="form-control">
                        </div>

                        <button type="submit" class="btn btn-primary rounded-0">Register</button>

                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection