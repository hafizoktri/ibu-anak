@extends('index')

@section('title')
    Login Ibu    
@endsection

@section('content')
    <div class="container py-5">
        
        <div class="d-flex justify-content-center">
            <div class="card w-50">
                <div class="card-body">

                    <h3>Login Ibu</h3>

                    <form action="{{ route('login') }}" method="post" class="">
                        {{ csrf_field() }}
                        
                        {{-- <div class="form-group">
                            <input type="text" name="" id="" class="form-control">
                        </div> --}}
                        
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('email')}}</p>
                        </div>
        
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('password')}}</p>
                        </div>
        
                        {{-- <div class="form-group">
                            <input type="number" name="" id="" class="form-control">
                        </div> --}}

                        <button type="submit" class="btn btn-danger rounded-0 px-5" name="button">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection