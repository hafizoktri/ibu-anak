@extends('index')

@section('title')
    Dashboard ibu
@endsection

@section('content')
    <section class="">

        {{-- <p>last sign in at {{ auth()->user()->login }}</p> --}}

        <div class="sidenav">
            <ul class="list-unstyled">
                <li class="py-2 btn-danger">
                    <a href="/forum" class="py-2 px-4 text-white font-weight-bold text-decoration-none">FORUM</a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">LOGBOOK</a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">CALL PERAWAT</a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">REMINDER</a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">KUESIONER</a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">MATERI EDUKASI</a>
                </li>
            </ul>
        </div>

        <div class="main-dash py-2">
            <h3 class="text-center">Selamat Datang, {{Auth::user()->name}}</h3>
    
        </div>

    </section>
@endsection