<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title></title>

    <style>
        .list-group{
            overflow-y: scroll;
            height: 200px;
            background-color: white;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <div class="container">
        <div class="row" id="app">
            
            <div class="offest-4 col-4 offset-sm-1 col-sm-10">
                <li class="list-group-item active">Chat Room
                    <span class="badge badge-danger">@{{ numberOfUsers }}</span>
                </li>

                <div class="badge badge-pill badge-light">@{{ typing }}</div>

                <ul class="list-group" v-chat-scroll>
                    <message v-for="value,index in chat.message" 
                        :key= value.index 
                        :color= chat.color[index]
                        :user = chat.user[index]
                        :time = chat.time[index]
                        >

                        @{{ value }}
                    </message>
                </ul>

                <input type="text" name="" id="" class="form-control my-2" 
                placeholder="Type your message here..." v-model="message" v-on:keyup.enter='send'>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>