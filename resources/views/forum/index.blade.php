@extends('index')

@section('title')
    Forum Perawatan 
@endsection

@section('content')
    <section class="container py-5">

        <h3 class="text-center">Forum perawatan</h3>
        
        <div class="row">
            <div class="col-sm-4 text-center">
                <a href="{{route('forum.create')}}" class="btn btn-primary text-decoration-none rounded-0">Buat Thread Baru</a>
            </div>
    
            <div class="col-sm-8">
                @foreach ($forums as $forum)
                    <div class="bg-white my-3 border p-3">
                        <a href="{{route('forum.show',$forum->id)}}" class="text-decoration-none">
                            <h4 class="font-weight-bold text-dark">{{$forum->title}}</h4>
                            <p class="font-weight-bold text-dark">{!!$forum->body!!}</p>
                            <p class="text-primary text-right">{{$forum->user->name}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection