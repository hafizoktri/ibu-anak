@extends('index')

@section('title')
    Ibu Anak - {{$thread->title}}
@endsection

@section('content')
    <section class="container py-5">

        <div class="row">
            <div class="col-sm-8">
                <div class="bg-white p-3 border">
                    <h4>{{$thread->title}}</h4>
    
                    
                    <date class="float-right">{{date('j M Y h:m', strtotime($thread->created_at))}}</date>
                    <p>{{$thread->user->name}}</p>
                    
                    <hr>
    
                    <article class="">
                        {!!$thread->body!!}
                    </article>
                </div>
    
                <div class="bg-white p-3 my-3 border">
                    {{-- reply form --}}
    
                    <form action="{{route('reply.store', $thread->id)}}" method="post">
                        {{ csrf_field() }}
    
                        <p>Reply disini...</p>
                        
                        <div class="form-group">
                            <textarea name="body" id="body" cols="5" rows="2" class=""></textarea>
                        </div>
    
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Reply</button>
                        </div>
    
                        <script type="text/javascript">
                            CKEDITOR.replace('body');
                        </script>
                    </form>
                </div>

                @foreach ($replies as $reply)
                <div class="my-3">
                    <div class="p-3 bg-white border">
                        <div class="">
                            <p class="font-weight-bold">{{$reply->user->name}}</p>
                            <p>{!!$reply->body!!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            {{-- sidebar --}}
            <div class="col-sm-4">
                <h5 class="font-weight-bold">Thread Lainnya</h5>
                @foreach ($threads as $th)
                    <div class="bg-white p-3 my-3 border">
                        <div class="">
                            <a href="{{route('forum.show',$th->id)}}" class="text-dark font-weight-bold">{{$th->title}}</a>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>
@endsection