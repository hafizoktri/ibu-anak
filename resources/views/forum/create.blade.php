@extends('index')

@section('title')
    Create New Thread
@endsection

@section('content')
    <section class="container py-5">
        <h3 class="text-center">Buat Thread Baru</h3>

        <div class="d-flex justify-content-center">
            <div class="w-75">
                <form action="{{route('forum.store')}}" method="post" enctype="multipart/form-data" class="">
                    {{ csrf_field() }}
    
                    {{-- masukin ckeditor --}}
    
                    <div class="form-group">
                        <input type="text" name="title" id="title" class="form-control rounded-0" placeholder="Masukkan Judul Pertanyaan Disini">
                    </div>
    
                    <div class="form-group">
                        <textarea name="body" id="body" cols="10" rows="5"></textarea>
                    </div>
    
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary rounded-0">Post</button>
                    </div>
    
                    <script type="text/javascript">
                        CKEDITOR.replace('body');
                    </script>
                </form>
            </div>

        </div>
    </section>
@endsection