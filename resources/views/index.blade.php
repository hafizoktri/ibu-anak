<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>

    <style>
        .bg-header{
            background-color: pink;
        }

        .btn-pink{
            background-color: #d34c88;
        }

        .btn-pink:hover{
            background-color: #ff78a9;
        }

        .footer{
            position: absolute;
            clear: both;
            z-index: 10;
            bottom: 0;
            background-color: pink;
            width: 100%;
            height: 80px;
            margin-top: -80px;
        }

        .sidenav{
            position: fixed;
            height: 100%;
            width: 225px;
            background-color: #d34c88;
            display: block;
        }

        .main-dash{
            margin-left: 225px;
        }
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
</head>

<body>

    <header class="bg-header py-2">
        <div class="d-flex justify-content-between align-items-center">
            <h3 class="m-2"><a href="/" class="text-white text-decoration-none">Ibu Anak</a></h3>
        
            <div class="float-right mr-3">
                @if (auth()->check())
                    <a href="{{route('dashboard')}}" class="py-2 px-4 btn-pink text-white font-weight-bold text-decoration-none">Dashboard</a>
                    <a href="{{route('logout')}}" class="py-2 px-4 btn-danger text-white font-weight-bold text-decoration-none">LOGOUT</a>
                @else
                    <a href="{{route('login')}}" class="py-2 px-4 btn-pink text-white font-weight-bold text-decoration-none">LOGIN</a>
                @endif
            </div>
        </div>
        
    </header>

    @yield('content')

    <footer class="page-footer py-3">
            <p class="text-white text-center">2019 Ibu Anak</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>