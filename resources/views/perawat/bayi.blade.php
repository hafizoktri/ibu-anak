@extends('perawat.dashboard')

@section('title')
    Pengkajian
@endsection

@section('dash')
    <section class="container py-5">

        <div class="d-flex justify-content-center">
            <div class="card w-50">
                <div class="card-body">

                    <form action="" method="post" class="">
                        {{ csrf_field() }}
                        
                        <h3>Bayi</h3>
                        
                        <div class="form-group">
                            <label for="name_bayi">Nama Bayi</label>
                            <input type="text" name="name_bayi" id="name_bayi" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('name_bayi')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="bplace">Tempat Tanggal Lahir</label>
                            <div class="d-flex justify-content-center">
                                <div class="w-50 mr-3">
                                    <input type="text" name="bplace" id="bplace" class="form-control">
                                </div>
                                <div class="w-50">
                                    <input type="date" name="bdate" id="bdate" class="form-control">
                                </div>
                            </div>
                            <p class="text-danger font-italic">{{$errors->first('bplace')}}</p>
                            <p class="text-danger font-italic">{{$errors->first('bdate')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="age">Usia Gestasi</label>
                            <input type="number" name="age" id="age" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('age')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="bblahir">Berat Badan Lahir</label>
                            <input type="number" name="bblahir" id="bblahir" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('bblahir')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="bbsekarang">Berat Badan Sekarang</label>
                            <input type="number" name="bbsekarang" id="bbsekarang" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('bbsekarang')}}</p>
                        </div>
                        
                        <h3>Orang Tua</h3>
    
                        <div class="form-group">
                            <label for="name_ibu">Nama Ibu</label>
                            <input type="text" name="name_ibu" id="name_ibu" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('name_ibu')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="name_ayah">Nama Ayah</label>
                            <input type="text" name="name_ayah" id="name_ayah" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('name_ayah')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="number" name="umur" id="umur" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('umur')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="agama">Agama</label>
                            <select name="agama" id="agama" class="form-control">
                                <option value="islam">Islam</option>
                                <option value="katolik">Katolik</option>
                                <option value="protestan">Protestan</option>
                                <option value="hindu">Hindu</option>
                                <option value="buddha">Buddha</option>
                                <option value="kepercayaan">Kepercayaan</option>
                            </select>
                        </div>
    
                        <div class="form-group">
                            <label for="suku">Suku Bangsa</label>
                            <input type="text" name="suku" id="suku" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('suku')}}</p>
                        </div>
    
                        <div class="form-group">
                            <label for="pendidikan">Tingkat Pendidikan</label>
                            <input type="text" name="pendidikan" id="pendidikan" class="form-control">
                            <p class="text-danger font-italic">{{$errors->first('pendidikan')}}</p>
                        </div>

                        <label for="email" hidden>Email</label>
                        <input type="email" name="email" id="email" class="form-control" hidden>
    
                        <button type="submit" class="btn btn-primary">Simpan Data</button>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection