@extends('index')

@section('title')
    Login Perawat
@endsection

@section('content')
    <div class="container py-3">

        <div class="d-flex justify-content-center">
            <div class="card w-50">
                <div class="card-body">

                    <h3>Login Perawat</h3>

                    <form action="{{ route('login') }}" method="post" class="">
                        {{ csrf_field() }}
                        
                        {{-- <div class="form-group">
                            <input type="text" name="" id="" class="form-control">
                        </div> --}}
                        
                        <div class="form-group">
                            <label for="email">Username</label>
                            <input type="email" name="email" id="email" class="form-control">
                            @if ($errors->has('email'))
                                {{$errors->first('email')}}
                            @endif
                        </div>
        
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                            @if ($errors->has('password'))
                                {{$errors->first('password')}}
                            @endif
                        </div>
        
                        <button type="submit" class="btn btn-danger rounded-0 px-5" name="button">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection