@extends('perawat.dashboard')

@section('title')
    Log Book Perawat    
@endsection

@section('dash')

    <section class="container py-3">

        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Pasien</th>
                    <th>Tanggal</th>
                    <th>Login</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($moms as $mom)
                <tr>
                    <td>{{$mom->id}}</td>
                    <td>{{$mom->name}}</td>
                    <td>{{$mom->created_at}}</td>
                    <td>{{$mom->login}}</td>
                    <td></td>  
                </tr>
                @endforeach
            </tbody>
        </table>

        {{$moms->links()}}

    </section>
@endsection