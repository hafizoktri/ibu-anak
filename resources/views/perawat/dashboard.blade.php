@extends('index')

@section('title')
    Dashboard Perawat
@endsection

@section('content')
    <section class="">

        <div class="sidenav">
            <ul class="list-unstyled">
                <li class="py-2 btn-danger">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">FORUM</a>
                </li>
        
                <li class="py-2 btn-primary">
                    <a href="{{route('signup')}}" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        DAFTAR IBU BAYI
                    </a>
                </li>
        
                <li class="py-2 btn-pink">
                    <a href="{{route('kaji')}}" class="py-2 px-4 btn-pink text-white font-weight-bold text-decoration-none">
                        ISI DATA BAYI
                    </a>
                </li>
        
                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        DATA KESIAPAN EDUKASI
                    </a>
                </li>
        
                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        CALL IBU BAYI
                    </a>
                </li>
        
                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        MATERI EDUKASI
                    </a>
                </li>
                
                <li class="py-2 btn-pink">
                    <a href="{{route('logbook')}}" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        LOG BOOK IBU
                    </a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        KUESIONER IBU
                    </a>
                </li>

                <li class="py-2 btn-pink">
                    <a href="#" class="py-2 px-4 text-white font-weight-bold text-decoration-none">
                        REMINDER IBU
                    </a>
                </li>
            </ul>
        </div>

        <div class="text-red">
            {{session('message')}}
        </div>

        <div class="main-dash py-2">
            <h3 class="text-center">Selamat Datang Perawat</h3>

            @yield('dash')
        </div>

    </section>
@endsection