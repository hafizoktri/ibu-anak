<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//dashboard ibu
Route::get('/', 'MotherController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'MotherController@dashboard')->name('dashboard');
});

//login ibu
Route::get('/login', 'LoginController@login')->name('login');
Route::post('/login', 'LoginController@postlogin');
Route::get('/logout', 'LoginController@logout')->name('logout');

//Register Ibu
Route::get('/register', 'RegisterController@index')->name('signup');
Route::post('/register', 'RegisterController@register')->name('register');

//login perawat
Route::get('/login-nurse', 'LoginNurseController@login')->name('login-nurse');
Route::post('/login-nurse', 'LoginNurseController@postlogin');
Route::get('/logout-nurse', 'LoginNurseController@logout')->name('logout-nurse');

//register perawat
Route::get('/register-nurse', 'RegisterNurseController@index')->name('signup-nurse');
Route::post('/register-nurse', 'RegisterNurseController@register')->name('register-nurse');

//Perawat
Route::get('/perawat', 'NurseController@dashboard');
Route::get('/logbook', 'NurseController@log')->name('logbook');

//pengkajian
Route::get('/pengkajian', 'NurseController@pengkajian')->name('kaji');
Route::post('/pengkajian', 'NurseController@storekaji');

//chat
Route::get('/chat', 'ChatController@index');
Route::post('/send', 'ChatController@send');

Route::get('messages', 'ChatController@fetchMessages');
Route::post('messages', 'ChatController@sendMessage');


//forum
Route::get('/forum', 'ForumController@index');
Route::get('/forum/create', 'ForumController@create')->name('forum.create');
Route::post('/forum/store', 'ForumController@store')->name('forum.store');
Route::get('/forum/{forum}', 'ForumController@show')->name('forum.show');

Route::post('/forum/reply/{forum_id}', 'ReplyController@store')->name('reply.store');
