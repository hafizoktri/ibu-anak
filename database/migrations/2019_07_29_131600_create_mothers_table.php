<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMothersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mothers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name_bayi');            
            $table->string('bplace');
            $table->date('bdate');
            $table->tinyInteger('age');
            $table->decimal('bblahir');
            $table->decimal('bbsekarang');

            $table->string('name_ibu');
            $table->string('name_ayah');
            $table->tinyInteger('umur');
            $table->string('agama');
            $table->string('suku');
            $table->string('pendidikan');

            $table->string('email')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mothers');
    }
}
