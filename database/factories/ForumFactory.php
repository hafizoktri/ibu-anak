<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Forum;
use App\Reply;
use Faker\Generator as Faker;

    $factory->define(App\User::class, function (Faker $faker) {
        static $password;

        return [
            'name' => $faker->name,
            'username' => $faker->userName,
            'email' => $faker->unique()->safeEmail,
            'password' => $password ?: $password = bcrypt('password'),
            'remember_token' => str_random(10),
        ];
    });
    
    $factory->define(App\Forum::class, function(Faker $faker){
        return [
            'user_id' => function(){
                return factory('App\User')->create()->id;
            },
            'title' => $faker->sentence,
            'body' => $faker->paragraph
        ];
    });

    $factory->define(App\Reply::class, function(Faker $faker){
        return [
            'forum_id' => function(){
                return factory('App\Forum')->create()->id;
            },
            'user_id' => function(){
                return factory('App\User')->create()->id;
            },
            'body' => $faker->paragraph
        ];
    });