<?php

namespace App;

use Auth as Authenticable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Mother extends Model
{
    use Notifiable;

    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];
}
