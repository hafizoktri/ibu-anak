<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function replies(){
        return $this->hasMany('App\Reply');
    }

    // public function addReply($reply){
    //     $reply = $this->replies()->create($reply);

    //     return $reply;
    // }
}
