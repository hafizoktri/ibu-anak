<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mother;

class MotherController extends Controller
{
    public function index(){
        return view('home');
    }

    public function dashboard(){
        return view('ibu.dashboard');
    }
}
