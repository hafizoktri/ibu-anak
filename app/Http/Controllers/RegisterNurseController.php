<?php

namespace App\Http\Controllers;

use Auth;
use App\Nurse;
use App\User;
use Illuminate\Http\Request;

class RegisterNurseController extends Controller
{
    public function index(){
        return view('perawat.register');
    }

    public function register(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:64|unique:mothers',
            'username' => 'required|string|max:32',
            'password' => 'required|string|min:6',
        ]);

        $nurse = Nurse::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password),
        ]);

        // Auth::loginUsingId($nurse->id);

        return redirect('/');
    }
}
