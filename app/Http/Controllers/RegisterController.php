<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Mother;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function index(){
        return view('ibu.register');
    }

    public function register(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:64|unique:mothers',
            'username' => 'required|string|max:32',
            'password' => 'required|string|min:6',
        ]);

        $ibu = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password),
        ]);

        Auth::loginUsingId($ibu->id);

        return redirect('/');
    }
}
