<?php

namespace App\Http\Controllers;

use App\Mother;
use App\Nurse;
use App\User;
use Illuminate\Http\Request;

class NurseController extends Controller
{
    public function dashboard(){
        $moms = Mother::all();

        return view('perawat.dashboard', compact('moms'));
    }

    public function pengkajian(){
        return view('perawat.bayi');
    }

    public function storekaji(Request $request){
        $mother = new Mother;

        $mother->name_bayi = $request->name_bayi;
        $mother->bplace = $request->bplace;
        $mother->bdate = $request->bdate;
        $mother->age = $request->age;
        $mother->bblahir = $request->bblahir;
        $mother->bbsekarang = $request->bbsekarang;

        $mother->name_ibu = $request->name_ibu;
        $mother->name_ayah = $request->name_ayah;
        $mother->umur = $request->umur;
        $mother->agama = $request->agama;
        $mother->suku = $request->suku;
        $mother->pendidikan = $request->pendidikan;

        $mother->email = '';

        $mother->save();

        return redirect('/perawat')->with('message', 'Data bayi telah dimasukkan');
    }

    public function log(){
    
        $moms = User::paginate(10);

        return view('perawat.log', compact('moms'));
    }
}
