<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginNurseController extends Controller
{
    public function login(){
        return view('perawat.login');
    }

    public function postlogin(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/perawat');
            dd($request);
        }
        return back()->withInput();
    }

    public function logout(){
        Auth::logout();

        return redirect('/');
    }
}
