<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Reply;
use App\Forum;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function store(Request $request, $forum_id){

        $reply = new Reply;

        $forum = Forum::find($forum_id);

        $reply->user_id = auth()->id();
        $reply->forum()->associate($forum);
        $reply->body = $request->body;

        $reply->save();

        return redirect()->route('forum.show', [$forum->id]);
    }
}
