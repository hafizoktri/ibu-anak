<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Forum;
use App\Reply;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function index(){
        $forums = Forum::all();
        return view('forum.index', compact('forums'));
    }

    public function create(){
        return view('forum.create');
    }

    public function store(Request $request){
        $newthread = new Forum;

        $newthread->user_id = auth()->id();
        $newthread->title = $request->title;
        $newthread->body = $request->body;

        $newthread->save();

        return redirect('/forum/'.$newthread->id);
    }

    public function show($id){
        $thread = Forum::findOrFail($id);

        $threads = Forum::all();

        $replies = Reply::where('forum_id', $id)->orderBy('created_at', 'desc')->get();
        
        return view('forum.thread', compact('thread','threads', 'replies'));
        //return $forum;
    }

    public function reply(Request $request){
        //
    }
}
