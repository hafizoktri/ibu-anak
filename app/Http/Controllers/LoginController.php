<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Mother;

use Session;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(){
        return view('ibu.login');
    }

    public function postlogin(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/dashboard');
            dd($request);
        }
        return back()->withInput();
    }

    public function logout(){
        Auth::logout();

        return redirect('/');
    }

    function authenticated(Request $request, $user){
        $user->login = Carbon::now();
        $user->save();
    }
}
