<?php

namespace App\Http\Controllers;

use App\Events\ChatEvent;
use App\User;
use App\Mother;
use App\Chat;
use Auth;
use Illuminate\Http\Request;

class ChatController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        return view('chat');
    }

    public function send(Request $request){
        
        $user = User::find(Auth::id());
        event(new ChatEvent($request->message,$user));
    }

    public function fetchMessages(){
        return Chat::with('user')->get();
    }

    public function sendMessage(Request $request){
        $user = Auth::user();

        $message = $user->message()->create([
            'message' => $request->input('message')
        ]);

        return ['status' => 'Message sent'];
    }

    // public function send(){
        
    //     $message = "hello";
    //     $user = User::find(Auth::id());
    //     event(new ChatEvent($message,$user));
    // }
}
